﻿'Import fungsi fungsi ODBC
Imports System.Data.Odbc
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text
Public Class login
    'Inisialisasi variabel pendukung odbc
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    'Pembuatan procedure koneksi menggunakan sub
    Sub koneksi()
        lokasiDB = "Dsn=dsn_tendbil;server=localhost;uid=root;database=db_tendbil;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub
    'Pembuatan procedure login admin menggunakan sub
    Sub loginadmin()
        If TextBox1.Text = "" Or TextBox2.Text = "" Then
            MsgBox("Harap Isi Username dan Password!", MsgBoxStyle.Critical)
        Else
            Call koneksi()
            cmd = New OdbcCommand("SELECT * FROM tb_admin WHERE username='" & TextBox1.Text & "'and password='" & TextBox2.Text & "'", conn)
            rd = cmd.ExecuteReader
            rd.Read()
            If rd.HasRows Then
                TextBox1.Text = ""
                TextBox2.Text = ""
                If rd.Item("level") = "2" Then
                    menuadmin.Label7.Text = rd.Item("username")
                    menuadmin.Label8.Text = rd.Item("password")
                    menuadmin.Label3.Text = rd.Item("nama")
                    menuadmin.Label5.Text = rd.Item("id_users")
                    Me.Visible = False
                    menuadmin.Show()
                    menuadmin.kondisibutton()
                    menuadmin.greet()
                    start.MdiParent = menuadmin
                    start.Show()
                    Me.Hide()
                    AdministratorToolStripMenuItem.BackColor = Color.IndianRed
                    StudentToolStripMenuItem.BackColor = Color.Transparent
                    TeacherToolStripMenuItem.BackColor = Color.Transparent
                    Label5.Text = "(Administrator)"
                    TextBox1.Text = ""
                    TextBox2.Text = ""
                ElseIf rd.Item("level") = "1" Then
                    menuheadmaster.Label7.Text = rd.Item("username")
                    menuheadmaster.Label8.Text = rd.Item("password")
                    menuheadmaster.Label3.Text = rd.Item("nama")
                    menuheadmaster.Label5.Text = rd.Item("id_users")
                    Me.Visible = False
                    menuheadmaster.Show()
                    menuheadmaster.greet()
                    humans.MdiParent = menuheadmaster
                    humans.Show()
                    humans.refreshpage()
                    Me.Hide()
                    AdministratorToolStripMenuItem.BackColor = Color.IndianRed
                    StudentToolStripMenuItem.BackColor = Color.Transparent
                    TeacherToolStripMenuItem.BackColor = Color.Transparent
                    Label5.Text = "(Administrator)"
                    TextBox1.Text = ""
                    TextBox2.Text = ""
                End If
            Else
                MsgBox("Username atau Password salah!", MsgBoxStyle.Critical)
            End If
        End If
    End Sub
    'Pembuatan procedure login teacher menggunakan sub
    Sub loginteacher()
        If TextBox1.Text = "" Or TextBox2.Text = "" Then
            MsgBox("Harap Isi Username dan Password!", MsgBoxStyle.Critical)
        Else
            Call koneksi()
            cmd = New OdbcCommand("SELECT * FROM tb_teacher WHERE username='" & TextBox1.Text & "'and password='" & TextBox2.Text & "'", conn)
            rd = cmd.ExecuteReader
            rd.Read()
            If rd.HasRows Then
                Me.Visible = False
                menuteacher.Show()
                menuteacher.greet()
                Me.Hide()
                TextBox1.Text = ""
                TextBox2.Text = ""
                menuteacher.Label7.Text = rd.Item("username")
                menuteacher.Label8.Text = rd.Item("password")
                menuteacher.Label3.Text = rd.Item("nama")
                menuteacher.Label5.Text = rd.Item("id_teacher")
                mystudent.Label10.Text = rd.Item("id_class")
                mystudent.MdiParent = menuteacher
                mystudent.Show()
                mystudent.refreshpage2()
                mystudent.refreshpage3()
                AdministratorToolStripMenuItem.BackColor = Color.IndianRed
                StudentToolStripMenuItem.BackColor = Color.Transparent
                TeacherToolStripMenuItem.BackColor = Color.Transparent
                Label5.Text = "(Administrator)"
                TextBox1.Text = ""
                TextBox2.Text = ""
            Else
                MsgBox("Username atau Password salah", MsgBoxStyle.Critical, "Warning")
                TextBox1.Text = ""
                TextBox2.Text = ""
            End If
        End If
    End Sub
    'Pembuatan procedure login student menggunakan sub
    Sub loginstudent()
        If TextBox1.Text = "" Or TextBox2.Text = "" Then
            MsgBox("Harap Isi Username dan Password!", MsgBoxStyle.Critical)
        Else
            Call koneksi()
            cmd = New OdbcCommand("SELECT * FROM tb_student WHERE username='" & TextBox1.Text & "'and password='" & TextBox2.Text & "'", conn)
            rd = cmd.ExecuteReader
            rd.Read()
            If rd.HasRows Then
                menustudent.Show()
                menustudent.greet()
                Me.Hide()
                TextBox1.Text = ""
                TextBox2.Text = ""
                menustudent.Label7.Text = rd.Item("username")
                menustudent.Label8.Text = rd.Item("password")
                menustudent.Label3.Text = rd.Item("nama")
                menustudent.Label5.Text = rd.Item("id_student")
                mygrade.Label10.Text = rd.Item("id_student")
                mygrade.MdiParent = menustudent
                mygrade.Show()
                mygrade.refreshpage()
            Else
                MsgBox("Username atau Password salah", MsgBoxStyle.Critical, "Warning")
                TextBox1.Text = ""
                TextBox2.Text = ""
            End If
        End If
    End Sub
    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        'fungsi see password
        PictureBox2.Visible = False
        PictureBox3.Visible = True
        TextBox2.PasswordChar = ""
    End Sub

    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click
        'fungsi see password
        PictureBox3.Visible = False
        PictureBox2.Visible = True
        TextBox2.PasswordChar = "*"
    End Sub
    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        'seleksi pilihan aksi pada button login
        If AdministratorToolStripMenuItem.BackColor = Color.IndianRed Then
            Call loginadmin()
        ElseIf TeacherToolStripMenuItem.BackColor = Color.IndianRed Then
            Call loginteacher()
        Else
            Call loginstudent()
        End If
    End Sub
  Private Sub AdministratorToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AdministratorToolStripMenuItem.Click
        'Jika di klik menu admin,maka berubah warna backcolor(untuk acuan aksi button login)
        AdministratorToolStripMenuItem.BackColor = Color.IndianRed
        StudentToolStripMenuItem.BackColor = Color.Transparent
        TeacherToolStripMenuItem.BackColor = Color.Transparent
        Label5.Text = "(Administrator)"
        TextBox1.Text = ""
        TextBox2.Text = ""
    End Sub

    Private Sub TeacherToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles TeacherToolStripMenuItem.Click
        'Jika di klik menu teacher,maka berubah warna backcolor(untuk acuan aksi button login)
        AdministratorToolStripMenuItem.BackColor = Color.Transparent
        StudentToolStripMenuItem.BackColor = Color.Transparent
        TeacherToolStripMenuItem.BackColor = Color.IndianRed
        Label5.Text = "(Teacher)"
        TextBox1.Text = ""
        TextBox2.Text = ""
    End Sub

    Private Sub StudentToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles StudentToolStripMenuItem.Click
        'Jika di klik menu student,maka berubah warna backcolor(untuk acuan aksi button login)
        AdministratorToolStripMenuItem.BackColor = Color.Transparent
        StudentToolStripMenuItem.BackColor = Color.IndianRed
        TeacherToolStripMenuItem.BackColor = Color.Transparent
        Label5.Text = "(Student)"
        TextBox1.Text = ""
        TextBox2.Text = ""
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        'button reset
        TextBox1.Text = ""
        TextBox2.Text = ""
    End Sub

    Private Sub Label4_Click(sender As System.Object, e As System.EventArgs) Handles Label4.Click
        'button quit app
        Dim pesan As String = MsgBox("Are you sure want to quit the application?", MsgBoxStyle.Question + MsgBoxStyle.OkCancel)
        If pesan = vbOK Then
            End
        End If
    End Sub

    Private Sub login_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class