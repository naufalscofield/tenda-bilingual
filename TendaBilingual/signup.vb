﻿Imports System.Data.Odbc
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text
Public Class signup
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub koneksi()
        lokasiDB = "Dsn=dsn_tendbil;server=localhost;uid=root;database=db_tendbil;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub
    Sub refreshtextbox()
        TextBox12.Text = ""
        TextBox11.Text = ""
        TextBox10.Text = ""
        TextBox9.Text = ""
        ComboBox6.Text = ""
        ComboBox5.Text = ""

        TextBox8.Text = ""
        TextBox7.Text = ""
        TextBox6.Text = ""
        TextBox5.Text = ""
        ComboBox4.Text = ""
        ComboBox3.Text = ""

        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        ComboBox1.Text = ""
        ComboBox2.Text = ""
    End Sub

    Private Sub signup_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Button1.BackColor = Color.IndianRed

        Call koneksi()

        cmd = New OdbcCommand("SELECT * FROM tb_class", conn)
        rd = cmd.ExecuteReader
        While rd.Read()
            ComboBox2.Items.Add(rd.Item("nama_class"))
            ComboBox3.Items.Add(rd.Item("nama_class"))
        End While
        rd.Close()
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Call koneksi()

        cmd = New OdbcCommand("select id_class,kode_class from `tb_class` where nama_class  = '" & ComboBox2.Text & "' ", conn)
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            Label1.Text = rd.Item("id_class")
        End If
    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        Call refreshtextbox()
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        If TextBox1.Text = "" Or TextBox2.Text = "" Or TextBox3.Text = "" Or TextBox4.Text = "" Or ComboBox1.Text = "" Or ComboBox2.Text = "" Then
            MsgBox("Please complete the form!", MsgBoxStyle.Information)
        Else
            Dim msg As String = MsgBox("Are you sure those data is valid?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
            If msg = vbYes Then
                Call koneksi()
                Dim simpan As String = "INSERT INTO `tb_student` (nama,alamat,no_hp,email,jk,username,password,id_class) values ('" & TextBox1.Text & "','" & TextBox2.Text & "','" & TextBox3.Text & "','" & TextBox4.Text & "','" & ComboBox1.Text & "','','' , '" & Label1.Text & "')"
                cmd = New OdbcCommand(simpan, conn)
                cmd.ExecuteNonQuery()

                cmd = New OdbcCommand("SELECT MAX(id_student) as id_student FROM tb_student", conn)
                rd = cmd.ExecuteReader
                rd.Read()
                If rd.HasRows Then
                    Dim idakhir As Integer = rd.Item("id_student")
                    Dim simpan2 As String = "INSERT INTO `tb_grade` (id_student,id_class,grade1,grade2,grade3,overall) values ('" & idakhir & "', '" & Label1.Text & "','0','0','0','0')"
                    cmd = New OdbcCommand(simpan2, conn)
                    cmd.ExecuteNonQuery()
                    MsgBox("Data has been saved", MsgBoxStyle.Information, "Tenda Bilingual")
                End If


                Call refreshtextbox()
            End If
                End If
    End Sub
    Private Sub Label23_Click(sender As System.Object, e As System.EventArgs) Handles Label23.Click
        TextBox15.Text = TextBox2.Text
        Button15.Visible = True
        TextBox15.Visible = True
    End Sub

    Private Sub Button15_Click(sender As System.Object, e As System.EventArgs) Handles Button15.Click
        TextBox15.Visible = False
        Button15.Visible = False
        TextBox2.Text = TextBox15.Text
    End Sub
    Private Sub Button9_Click(sender As System.Object, e As System.EventArgs) Handles Button9.Click
        Call refreshtextbox()
    End Sub

    Private Sub Button8_Click(sender As System.Object, e As System.EventArgs) Handles Button8.Click
        If TextBox8.Text = "" Or TextBox7.Text = "" Or TextBox6.Text = "" Or TextBox5.Text = "" Or ComboBox4.Text = "" Or ComboBox3.Text = "" Then
            MsgBox("Please complete the form!", MsgBoxStyle.Information)
        Else
            Dim msg As String = MsgBox("Are you sure those data is valid?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
            If msg = vbYes Then
                Call koneksi()
                Dim simpan As String = "INSERT INTO `tb_teacher` (nama,alamat,no_hp,email,jk,username,password,id_class) values ('" & TextBox8.Text & "','" & TextBox7.Text & "','" & TextBox6.Text & "','" & TextBox5.Text & "','" & ComboBox4.Text & "','','' , '" & Label8.Text & "')"
                cmd = New OdbcCommand(simpan, conn)
                cmd.ExecuteNonQuery()
                MsgBox("Data has been saved", MsgBoxStyle.Information, "Tenda Bilingual")
                Call refreshtextbox()
            End If
        End If
    End Sub

    Private Sub Label22_Click(sender As System.Object, e As System.EventArgs) Handles Label22.Click
        TextBox14.Text = TextBox7.Text
        Button14.Visible = True
        TextBox14.Visible = True
    End Sub

    Private Sub Button14_Click(sender As System.Object, e As System.EventArgs) Handles Button14.Click
        TextBox14.Visible = False
        Button14.Visible = False
        TextBox7.Text = TextBox14.Text
    End Sub

    Private Sub Button17_Click(sender As System.Object, e As System.EventArgs) Handles Button17.Click
        Call refreshtextbox()
    End Sub

    Private Sub Button16_Click(sender As System.Object, e As System.EventArgs) Handles Button16.Click
        If TextBox12.Text = "" Or TextBox11.Text = "" Or TextBox10.Text = "" Or TextBox9.Text = "" Or ComboBox6.Text = "" Or ComboBox5.Text = "" Then
            MsgBox("Please complete the form!", MsgBoxStyle.Information)
        Else
            Dim msg As String = MsgBox("Are you sure those data is valid?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
            If msg = vbYes Then
                Call koneksi()
                Dim simpan As String = "INSERT INTO `tb_admin` (nama,alamat,no_hp,email,jk,username,password,level) values ('" & TextBox12.Text & "','" & TextBox11.Text & "','" & TextBox10.Text & "','" & TextBox9.Text & "','" & ComboBox6.Text & "','','' , '" & ComboBox5.Text & "')"
                cmd = New OdbcCommand(simpan, conn)
                cmd.ExecuteNonQuery()
                Dim msg2 As String = MsgBox("Data has been saved", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Tenda Bilingual")
                    Call refreshtextbox()
                End If
            End If
    End Sub

    Private Sub Label15_Click(sender As System.Object, e As System.EventArgs) Handles Label15.Click
        TextBox13.Text = TextBox11.Text
        Button13.Visible = True
        TextBox13.Visible = True
    End Sub

    Private Sub Button13_Click(sender As System.Object, e As System.EventArgs) Handles Button13.Click
        TextBox13.Visible = False
        Button13.Visible = False
        TextBox11.Text = TextBox13.Text
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Button1.BackColor = Color.IndianRed
        Button2.BackColor = Color.Indigo
        Button3.BackColor = Color.Indigo
        GroupBox1.Visible = True
        gbteacher.Visible = False
        gbadmin.Visible = False
        Call refreshtextbox()
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Button2.BackColor = Color.IndianRed
        Button1.BackColor = Color.Indigo
        Button3.BackColor = Color.Indigo
        gbteacher.Visible = True
        GroupBox1.Visible = False
        gbadmin.Visible = False
        Call refreshtextbox()
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        Button3.BackColor = Color.IndianRed
        Button2.BackColor = Color.Indigo
        Button1.BackColor = Color.Indigo
        gbadmin.Visible = True
        gbteacher.Visible = False
        GroupBox1.Visible = False
        Call refreshtextbox()
    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox3.SelectedIndexChanged
        Call koneksi()

        cmd = New OdbcCommand("select id_class,kode_class from `tb_class` where nama_class  = '" & ComboBox3.Text & "' ", conn)
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            Label8.Text = rd.Item("id_class")
        End If
    End Sub

    Private Sub TextBox10_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox10.KeyPress
        'validasi angka
        Dim strValid As String = "0123456789-.,"

        Dim x As Long = InStr(strValid, e.KeyChar)

        If x = 0 And Asc(e.KeyChar) <> 8 And Asc(e.KeyChar) <> 32 Then

            e.KeyChar = ""

        End If
    End Sub

    Private Sub TextBox6_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox6.KeyPress
        'validasi angka
        Dim strValid As String = "0123456789-.,"

        Dim x As Long = InStr(strValid, e.KeyChar)

        If x = 0 And Asc(e.KeyChar) <> 8 And Asc(e.KeyChar) <> 32 Then

            e.KeyChar = ""

        End If
    End Sub
    Private Sub TextBox3_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        'validasi angka
        Dim strValid As String = "0123456789-.,"

        Dim x As Long = InStr(strValid, e.KeyChar)

        If x = 0 And Asc(e.KeyChar) <> 8 And Asc(e.KeyChar) <> 32 Then

            e.KeyChar = ""

        End If
    End Sub

   
End Class