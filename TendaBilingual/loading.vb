﻿Public Class loading

    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
        ProgressBar1.Value += 1
        If ProgressBar1.Value = 32 Then
            Timer1.Dispose()
            Me.Visible = False
            Login.Show()
        End If
        If ProgressBar1.Value = 1 Then
            Label5.Visible = True
        End If
        If ProgressBar1.Value = 6 Then
            Label6.Visible = True
        End If
        If ProgressBar1.Value = 12 Then
            Label7.Visible = True
        End If
        If ProgressBar1.Value = 18 Then
            Label5.Visible = False
            Label6.Visible = False
            Label7.Visible = False
            Label3.Text = "Loading Application"
        End If
        If ProgressBar1.Value = 24 Then
            Label10.Visible = True
        End If
        If ProgressBar1.Value = 28 Then
            Label9.Visible = True
        End If
        If ProgressBar1.Value = 32 Then
            Label8.Visible = True
        End If
    End Sub

    Private Sub loading_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Timer1.Start()
        Me.FormBorderStyle = 0

        Dim p As New Drawing2D.GraphicsPath()
        Dim Pojok As Integer

        Pojok = 250

        p.AddLine(0, 0, Me.Width - Pojok, 0)
        p.AddArc(New Rectangle(Me.Width - Pojok, 0, Pojok, Pojok), -90, 90)
        p.AddLine(Me.Width, Pojok, Me.Width, Me.Height)
        p.AddLine(Me.Width - Pojok, Me.Height, Pojok, Me.Height)
        p.AddArc(New Rectangle(0, Me.Height - Pojok, Pojok, Pojok), 90, 90)

        Me.Region = New Region(p)
    End Sub
End Class