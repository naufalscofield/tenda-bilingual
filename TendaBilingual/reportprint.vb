﻿Imports System.Data.Odbc
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text
Public Class reportprint
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub refreshpage()
        GroupBox1.Visible = False
        ComboBox5.Text = ""
        Label1.Text = ""
        Label2.Text = ""
        Label4.Text = ""
        Label5.Text = ""
        Label6.Text = ""
        Label7.Text = ""
        Label8.Text = ""
        Button7.BackColor = Color.Indigo
        DataGridView1.DataSource = ""

    End Sub
    Public Sub addDGV()


        With DataGridView1
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 70
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 286
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 100
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 100
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).Width = 100
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(5).Width = 100
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        End With
    End Sub
    Sub koneksi()
        lokasiDB = "Dsn=dsn_tendbil;server=localhost;uid=root;database=db_tendbil;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub

    Private Sub reportprint_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Call koneksi()
        cmd = New OdbcCommand("SELECT * FROM tb_class", conn)
        rd = cmd.ExecuteReader
        While rd.Read()
            ComboBox5.Items.Add(rd.Item("nama_class"))
        End While
        rd.Close()
        Label3.Text = DateTimePicker1.Value.Year
    End Sub
    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        reportstd.Show()
        Call refreshpage()
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        reporttch.Show()
        Call refreshpage()
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        reportadm.Show()
        Call refreshpage()
    End Sub

    Private Sub Button7_Click(sender As System.Object, e As System.EventArgs) Handles Button7.Click
        Button7.BackColor = Color.IndianRed
        GroupBox1.Visible = True
    End Sub

    Private Sub ComboBox5_Click(sender As Object, e As System.EventArgs) Handles ComboBox5.Click
        ComboBox5.Text = ""
        Label1.Text = ""
        Label2.Text = ""
        Label4.Text = ""
        Label5.Text = ""
        Label6.Text = ""
        Label7.Text = ""
        Label8.Text = ""
        DataGridView1.DataSource = ""
    End Sub


    Private Sub ComboBox5_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox5.SelectedIndexChanged
        Call koneksi()
        cmd = New OdbcCommand("select * from `tb_class` where nama_class  = '" & ComboBox5.Text & "' ", conn)
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            Label2.Text = rd.Item("kode_class")
            Label4.Text = ComboBox5.Text
            Label8.Text = rd.Item("id_class")
        End If
        da = New OdbcDataAdapter("select tb_student.id_student,tb_student.nama,tb_grade.grade1,tb_grade.grade2,tb_grade.grade3,tb_grade.overall from `tb_student` left join tb_grade on tb_student.id_student = tb_grade.id_student where tb_grade.id_class = '" & Label8.Text & "' ", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "`tb_student`")
        DataGridView1.DataSource = (ds.Tables("`tb_student`"))
        Call addDGV()
    End Sub
    Private Sub DataGridView1_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Dim a As Object = DataGridView1.Rows(e.RowIndex).Cells(0).Value 'id
        Label1.Text = a
        Dim b As Object = DataGridView1.Rows(e.RowIndex).Cells(1).Value 'nama
        Label5.Text = b
        Dim c As Object = DataGridView1.Rows(e.RowIndex).Cells(5).Value 'score
        Label6.Text = c
        Dim nilai As Single = CSng(Label6.Text)
        Select Case nilai
            Case 241 To 300 : Label7.Text = "(A)"
            Case 201 To 240 : Label7.Text = "(B)"
            Case 161 To 200 : Label7.Text = "(C)"
            Case 121 To 160 : Label7.Text = "(D)"
            Case 0 To 120 : Label7.Text = "(E)"
        End Select
    End Sub

    

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        If Label1.Text = "" Or Label2.Text = "" Or Label4.Text = "" Or Label5.Text = "" Or Label6.Text = "" Or Label7.Text = "" Or Label8.Text = "" Then
            MsgBox("Select Class and Select Student!", MsgBoxStyle.Information)
        Else
            printcertificate.Show()
            printcertificate.Label4.Text = "No:/" & Label1.Text & "/" & Label2.Text & "/TENDA_BILINGUAL/" & Label3.Text
            printcertificate.Label5.Text = Label4.Text & ",Tenda Bilingual"
            printcertificate.Label7.Text = Label5.Text
            printcertificate.Label11.Text = Label6.Text
            printcertificate.Label12.Text = Label7.Text
        End If
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        reportgrd.Show()
        Call refreshpage()
    End Sub
End Class