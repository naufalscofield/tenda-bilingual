﻿Imports System.Data.Odbc
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text
Public Class classes
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub koneksi()
        lokasiDB = "Dsn=dsn_tendbil;server=localhost;uid=root;database=db_tendbil;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub
    Sub refreshpage()
        gbadd.Visible = True
        gbclass.Visible = False
        gbedit.Visible = False
        Button1.BackColor = Color.IndianRed
        Button2.BackColor = Color.Indigo
        TextBox12.Text = ""
        TextBox11.Text = ""
        TextBox10.Text = "00.00-00.00"


        Call koneksi()
        da = New OdbcDataAdapter("select tb_class.id_class, tb_class.kode_class,tb_class.nama_class,tb_class.jam_kelas,tb_teacher.nama from tb_class left join tb_teacher on tb_class.id_class = tb_teacher.id_class", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "`tb_class`")
        DataGridView1.DataSource = (ds.Tables("`tb_class`"))
        Button3.Visible = False
        Button4.Visible = False
        Label1.Text = ""
        Label2.Text = ""
        Call addDGV()


    End Sub
    Public Sub addDGV()


        With DataGridView1
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 50
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 120
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 185
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 100
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).Width = 303
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        End With
    End Sub
   
    Private Sub Button17_Click(sender As System.Object, e As System.EventArgs) Handles Button17.Click
        TextBox12.Text = ""
        TextBox11.Text = ""
        TextBox10.Text = "00.00-00.00"
    End Sub

    Private Sub Button16_Click(sender As System.Object, e As System.EventArgs) Handles Button16.Click
        Dim msg As String = MsgBox("Are you sure those data is valid?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
        If msg = vbYes Then
            Call koneksi()
            Dim simpan As String = "insert into tb_class (kode_class,nama_class,jam_kelas)values ('" & TextBox11.Text & "','" & TextBox12.Text & "','" & TextBox10.Text & "')"
            cmd = New OdbcCommand(simpan, conn)
            cmd.ExecuteNonQuery()
            MsgBox("Class has beed added", MsgBoxStyle.Information, "Tenda Bilingual")
            Call refreshpage()
        End If
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Button3.Visible = True
        Button4.Visible = True
        Dim a As Object = DataGridView1.Rows(e.RowIndex).Cells(0).Value 'id
        Label8.Text = a
        Dim b As Object = DataGridView1.Rows(e.RowIndex).Cells(2).Value 'clasname
        TextBox8.Text = b
        Dim c As Object = DataGridView1.Rows(e.RowIndex).Cells(1).Value 'code
        TextBox6.Text = c
        Dim d As Object = DataGridView1.Rows(e.RowIndex).Cells(3).Value 'schedule
        TextBox7.Text = d
    End Sub
    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Button1.BackColor = Color.IndianRed
        Button2.BackColor = Color.Indigo
        gbadd.Visible = True
        gbclass.Visible = False
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Button2.BackColor = Color.IndianRed
        Button1.BackColor = Color.Indigo
        gbadd.Visible = False
        gbclass.Visible = True
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        Dim pesan As String = MsgBox("Are you sure want to delete class " & TextBox8.Text & " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
        If pesan = vbYes Then
            Dim hapus As String = "delete from tb_class where id_class = '" & Label8.Text & "'"
            cmd = New OdbcCommand(hapus, conn)
            cmd.ExecuteNonQuery()
            MsgBox("Class has been deleted!", MsgBoxStyle.Information)
            Call refreshpage()
        End If
    End Sub

    Private Sub classes_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Call refreshpage()
    End Sub
    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        gbedit.Visible = True
    End Sub

    Private Sub Button6_Click(sender As System.Object, e As System.EventArgs) Handles Button6.Click
        TextBox8.Text = ""
        TextBox6.Text = ""
        TextBox7.Text = "00.00-00.00"
    End Sub

    Private Sub Label9_Click(sender As System.Object, e As System.EventArgs) Handles Label9.Click
        gbedit.Visible = False
    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        Dim pesan As String = MsgBox("Are you sure those data are valid?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
        If pesan = vbYes Then
            Call koneksi()
            Dim edit As String = "update tb_class set kode_class = '" & TextBox6.Text & "' , nama_class = '" & TextBox8.Text & "' , jam_kelas = '" & TextBox7.Text & "' where id_class = '" & Label8.Text & "'"
            cmd = New OdbcCommand(edit, conn)
            cmd.ExecuteNonQuery()
            MsgBox("Data has been updated!", MsgBoxStyle.Information)
            Call refreshpage()
        End If
    End Sub
End Class