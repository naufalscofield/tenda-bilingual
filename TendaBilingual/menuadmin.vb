﻿Public Class menuadmin
    'fungsi greet untuk menyeleksi ucapan lewat label
    Sub greet()
        If Val(Label6.Text) = "12" Or Val(Label6.Text) = "13" Or Val(Label6.Text) = "14" Or Val(Label6.Text) = "15" Or Val(Label6.Text) = "16" Or Val(Label6.Text) = "17" Then
            Label9.Text = "Good Afternoon"
        ElseIf Val(Label6.Text) = "3" Or Val(Label6.Text) = "4" Or Val(Label6.Text) = "5" Or Val(Label6.Text) = "6" Or Val(Label6.Text) = "7" Or Val(Label6.Text) = "8" Or Val(Label6.Text) = "9" Or Val(Label6.Text) = "10" Or Val(Label6.Text) = "11" Then
            Label9.Text = "Good Morning"
        Else
            Label9.Text = "Good Night"
        End If
    End Sub
    Sub kondisibutton()
        Button1.BackColor = Color.White
        Button1.ForeColor = Color.Indigo
        Button2.BackColor = Color.White
        Button2.ForeColor = Color.Indigo
        Button4.BackColor = Color.White
        Button4.ForeColor = Color.Indigo
        Button5.BackColor = Color.White
        Button5.ForeColor = Color.Indigo
        Button6.BackColor = Color.White
        Button6.ForeColor = Color.Indigo
        Button8.BackColor = Color.White
        Button8.ForeColor = Color.Indigo
        Button7.BackColor = Color.White
        Button7.ForeColor = Color.Indigo
        Button3.BackColor = Color.White
        Button3.ForeColor = Color.Indigo
    End Sub

    
    Private Sub menuadmin_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load 
        'memindahkan isi dtp.hour ke label
        Label6.Text = DateTimePicker1.Value.Hour
        'memindahkan isi dtp.date ke label status strip
        ToolStripStatusLabel1.Text = DateTimePicker1.Value.Date
    End Sub
    Private Sub Label4_Click(sender As System.Object, e As System.EventArgs) Handles Label4.Click
        'fungsi quit app
        Dim pesan As String = MsgBox("Are you sure want to quit the application?", MsgBoxStyle.Question + MsgBoxStyle.OkCancel)
        If pesan = vbOK Then
            End
        End If
    End Sub
    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        'memanggil form signup sbg form child dan men hide semua form child yang tampil
        signup.MdiParent = Me
        signup.Show()

        gateway.Hide()
        start.Hide()
        students.Hide()
        teachers.Hide()
        admins.Hide()
        classes.Hide()
        admprofile.Hide()
        reportprint.Hide()
        'penggantian warna button sesuai yang di klik
        Button1.BackColor = Color.Indigo
        Button1.ForeColor = Color.White
        Button2.BackColor = Color.White
        Button2.ForeColor = Color.Indigo
        Button4.BackColor = Color.White
        Button4.ForeColor = Color.Indigo
        Button5.BackColor = Color.White
        Button5.ForeColor = Color.Indigo
        Button6.BackColor = Color.White
        Button6.ForeColor = Color.Indigo
        Button8.BackColor = Color.White
        Button8.ForeColor = Color.Indigo
        Button7.BackColor = Color.White
        Button7.ForeColor = Color.Indigo
        Button3.BackColor = Color.White
        Button3.ForeColor = Color.Indigo

        signup.refreshtextbox()
    End Sub

    Private Sub Button6_Click(sender As System.Object, e As System.EventArgs) Handles Button6.Click
        'memanggil form student sbg form child dan men hide semua form child yang tampil
        students.MdiParent = Me
        students.Show()

        gateway.Hide()
        start.Hide()
        signup.Hide()
        teachers.Hide()
        admins.Hide()
        classes.Hide()
        admprofile.Hide()
        reportprint.Hide()
        'pengkondisian object yang ada di form students
        students.refreshpage()
        'penggantian warna button sesuai yang di klik
        Button6.BackColor = Color.Indigo
        Button6.ForeColor = Color.White
        Button1.BackColor = Color.White
        Button1.ForeColor = Color.Indigo
        Button4.BackColor = Color.White
        Button4.ForeColor = Color.Indigo
        Button5.BackColor = Color.White
        Button5.ForeColor = Color.Indigo
        Button2.BackColor = Color.White
        Button2.ForeColor = Color.Indigo
        Button8.BackColor = Color.White
        Button8.ForeColor = Color.Indigo
        Button7.BackColor = Color.White
        Button7.ForeColor = Color.Indigo
        Button3.BackColor = Color.White
        Button3.ForeColor = Color.Indigo


    End Sub
    Private Sub PictureBox3_Click(sender As System.Object, e As System.EventArgs) Handles PictureBox3.Click
        'fungsi quit app
        Dim pesan As String = MsgBox("Are you sure want to logout?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
        If pesan = vbYes Then
            login.Show()
            Me.Close()
        End If
    End Sub
    Private Sub PictureBox3_MouseHover(sender As Object, e As System.EventArgs) Handles PictureBox3.MouseHover
        'fungsi memunculkan label logout saat di hover
        Label2.Visible = True
    End Sub

    Private Sub PictureBox3_MouseLeave(sender As Object, e As System.EventArgs) Handles PictureBox3.MouseLeave
        'fungsi men hide label logout saat di mouse leave
        Label2.Visible = False
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        'memanggil form teacher sbg form child dan men hide semua form child yang tampil
        teachers.MdiParent = Me
        teachers.Show()

        gateway.Hide()
        start.Hide()
        signup.Hide()
        students.Hide()
        admins.Hide()
        classes.Hide()
        admprofile.Hide()
        reportprint.Hide()
        'pengkondisian object yang ada di form teacher
        teachers.refreshpage()
        'penggantian warna button sesuai yang di klik
        Button3.BackColor = Color.Indigo
        Button3.ForeColor = Color.White
        Button1.BackColor = Color.White
        Button1.ForeColor = Color.Indigo
        Button4.BackColor = Color.White
        Button4.ForeColor = Color.Indigo
        Button5.BackColor = Color.White
        Button5.ForeColor = Color.Indigo
        Button2.BackColor = Color.White
        Button2.ForeColor = Color.Indigo
        Button8.BackColor = Color.White
        Button8.ForeColor = Color.Indigo
        Button6.BackColor = Color.White
        Button6.ForeColor = Color.Indigo
        Button7.BackColor = Color.White
        Button7.ForeColor = Color.Indigo

    End Sub

    Private Sub Button7_Click(sender As System.Object, e As System.EventArgs) Handles Button7.Click
        'memanggil form admins sbg form child dan men hide semua form child yang tampil
        admins.MdiParent = Me
        admins.Show()

        gateway.Hide()
        start.Hide()
        signup.Hide()
        students.Hide()
        teachers.Hide()
        classes.Hide()
        admprofile.Hide()
        reportprint.Hide()
        'pengkondisian object yang ada di form admins
        admins.refreshpage()
        'penggantian warna button sesuai yang di klik
        Button7.BackColor = Color.Indigo
        Button7.ForeColor = Color.White
        Button1.BackColor = Color.White
        Button1.ForeColor = Color.Indigo
        Button4.BackColor = Color.White
        Button4.ForeColor = Color.Indigo
        Button5.BackColor = Color.White
        Button5.ForeColor = Color.Indigo
        Button2.BackColor = Color.White
        Button2.ForeColor = Color.Indigo
        Button8.BackColor = Color.White
        Button8.ForeColor = Color.Indigo
        Button6.BackColor = Color.White
        Button6.ForeColor = Color.Indigo
        Button3.BackColor = Color.White
        Button3.ForeColor = Color.Indigo
    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        'memanggil form classes sbg form child dan men hide semua form child yang tampil
        classes.MdiParent = Me
        classes.Show()

        gateway.Hide()
        start.Hide()
        signup.Hide()
        students.Hide()
        teachers.Hide()
        admins.Hide()
        admprofile.Hide()
        reportprint.Hide()
        'pengkondisian object yang ada di form classes
        classes.refreshpage()
        'penggantian warna button sesuai yang di klik
        Button5.BackColor = Color.Indigo
        Button5.ForeColor = Color.White
        Button1.BackColor = Color.White
        Button1.ForeColor = Color.Indigo
        Button4.BackColor = Color.White
        Button4.ForeColor = Color.Indigo
        Button7.BackColor = Color.White
        Button7.ForeColor = Color.Indigo
        Button2.BackColor = Color.White
        Button2.ForeColor = Color.Indigo
        Button8.BackColor = Color.White
        Button8.ForeColor = Color.Indigo
        Button6.BackColor = Color.White
        Button6.ForeColor = Color.Indigo
        Button3.BackColor = Color.White
        Button3.ForeColor = Color.Indigo
    End Sub

    Private Sub PictureBox2_Click(sender As System.Object, e As System.EventArgs) Handles PictureBox2.Click
        'memanggil form classes sbg form child dan men hide semua form child yang tampil
        admprofile.MdiParent = Me
        admprofile.Show()

        gateway.Hide()
        start.Hide()
        signup.Hide()
        students.Hide()
        teachers.Hide()
        admins.Hide()
        classes.Hide()
        reportprint.Hide()

    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        'memanggil form classes sbg form child dan men hide semua form child yang tampil
        reportprint.MdiParent = Me
        reportprint.Show()

        gateway.Hide()
        start.Hide()
        signup.Hide()
        students.Hide()
        teachers.Hide()
        admins.Hide()
        admprofile.Hide()
        classes.Hide()

        reportprint.refreshpage()
        'penggantian warna button sesuai yang di klik
        Button5.BackColor = Color.White
        Button5.ForeColor = Color.Indigo
        Button1.BackColor = Color.White
        Button1.ForeColor = Color.Indigo
        Button4.BackColor = Color.White
        Button4.ForeColor = Color.Indigo
        Button7.BackColor = Color.White
        Button7.ForeColor = Color.Indigo
        Button2.BackColor = Color.Indigo
        Button2.ForeColor = Color.White
        Button8.BackColor = Color.White
        Button8.ForeColor = Color.Indigo
        Button6.BackColor = Color.White
        Button6.ForeColor = Color.Indigo
        Button3.BackColor = Color.White
        Button3.ForeColor = Color.Indigo
    End Sub

  
    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        'memanggil form classes sbg form child dan men hide semua form child yang tampil
        gateway.MdiParent = Me
        gateway.Show()

        reportprint.Hide()
        start.Hide()
        signup.Hide()
        students.Hide()
        teachers.Hide()
        admins.Hide()
        admprofile.Hide()
        classes.Hide()

        reportprint.refreshpage()
        'penggantian warna button sesuai yang di klik
        Button4.BackColor = Color.Indigo
        Button4.ForeColor = Color.White
        Button1.BackColor = Color.White
        Button1.ForeColor = Color.Indigo
        Button5.BackColor = Color.White
        Button5.ForeColor = Color.Indigo
        Button7.BackColor = Color.White
        Button7.ForeColor = Color.Indigo
        Button2.BackColor = Color.White
        Button2.ForeColor = Color.Indigo
        Button8.BackColor = Color.White
        Button8.ForeColor = Color.Indigo
        Button6.BackColor = Color.White
        Button6.ForeColor = Color.Indigo
        Button3.BackColor = Color.White
        Button3.ForeColor = Color.Indigo
    End Sub
End Class