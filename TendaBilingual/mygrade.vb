﻿Imports System.Data.Odbc
Public Class mygrade
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub koneksi()
        lokasiDB = "Dsn=dsn_tendbil;server=localhost;uid=root;database=db_tendbil;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub
    Public Sub addDGV()


        With DataGridView1
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 70
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 286
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 100
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 100
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).Width = 100
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(5).Width = 100
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        End With
    End Sub
    Sub refreshpage()
        Call koneksi()
        da = New OdbcDataAdapter("select tb_student.username , tb_student.nama ,  tb_grade.grade1 , tb_grade.grade2 , tb_grade.grade3 , tb_grade.overall from tb_student LEFT JOIN tb_grade on tb_student.id_student = tb_grade.id_student where tb_student.id_student = '" & Label10.Text & "'", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "`tb_student`")
        DataGridView1.DataSource = (ds.Tables("`tb_student`"))
        Call addDGV()
    End Sub
    Private Sub mygrade_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Call refreshpage()
    End Sub
End Class