﻿Imports System.Data.Odbc
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text
Public Class edit
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub koneksi()
        lokasiDB = "Dsn=dsn_tendbil;server=localhost;uid=root;database=db_tendbil;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub

    Private Sub edit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Call koneksi()

        cmd = New OdbcCommand("SELECT * FROM tb_class", conn)
        rd = cmd.ExecuteReader
        While rd.Read()
            ComboBox5.Items.Add(rd.Item("nama_class"))
        End While
        rd.Close()
    End Sub

    Private Sub Label4_Click(sender As System.Object, e As System.EventArgs) Handles Label4.Click
        Me.Hide()
        students.Enabled = True
        menuadmin.Enabled = True
        students.ComboBox5.Text = ""
        students.Label1.Text = ""
        students.Label2.Text = ""
        students.Label3.Text = ""
        students.Label5.Text = ""
        students.Label6.Text = ""
        students.Label7.Text = ""
        students.Button1.Visible = False
        students.Button2.Visible = False
        students.Button3.Visible = False
        students.Button4.Visible = False
        students.dgv.DataSource = ""
    End Sub

    Private Sub Button16_Click(sender As System.Object, e As System.EventArgs) Handles Button16.Click
        Call koneksi()
        Dim msg As String = MsgBox("Are you sure those data is valid?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
        If msg = vbYes Then
            Dim edit As String = "UPDATE `tb_student` SET nama ='" & TextBox12.Text & "' , alamat = '" & TextBox11.Text & "' , no_hp = '" & TextBox10.Text & "',email = '" & TextBox9.Text & "',jk = '" & ComboBox6.Text & "' ,id_class = '" & Label2.Text & "' where `id_student` = '" & Label1.Text & "'"
            cmd = New OdbcCommand(edit, conn)
            cmd.ExecuteNonQuery()
            MsgBox("Data has been edited", MsgBoxStyle.Information, "Information")
            Me.Hide()
            students.Enabled = True
            menuadmin.Enabled = True
            students.ComboBox5.Text = ""
            students.Label1.Text = ""
            students.Label2.Text = ""
            students.Label3.Text = ""
            students.Label5.Text = ""
            students.Label6.Text = ""
            students.Label7.Text = ""
            students.Button1.Visible = False
            students.Button2.Visible = False
            students.Button3.Visible = False
            students.Button4.Visible = False
            students.dgv.DataSource = ""
        End If
    End Sub

    Private Sub gbadmin_Enter(sender As System.Object, e As System.EventArgs) Handles gbadmin.Enter
        Call koneksi()

        cmd = New OdbcCommand("select id_class from `tb_class` where nama_class  = '" & ComboBox5.Text & "' ", conn)
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            Label2.Text = rd.Item("id_class")
        End If
    End Sub

    Private Sub ComboBox5_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox5.SelectedIndexChanged
        Call koneksi()

        cmd = New OdbcCommand("select id_class from `tb_class` where nama_class  = '" & ComboBox5.Text & "' ", conn)
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            Label2.Text = rd.Item("id_class")
        End If
    End Sub

    Private Sub Label15_Click(sender As System.Object, e As System.EventArgs) Handles Label15.Click
        TextBox11.Text = TextBox13.Text
        Button13.Visible = True
        TextBox13.Visible = True
    End Sub

    Private Sub Button13_Click(sender As System.Object, e As System.EventArgs) Handles Button13.Click
        TextBox13.Visible = False
        Button13.Visible = False
        TextBox11.Text = TextBox13.Text
    End Sub

    Private Sub Button17_Click(sender As System.Object, e As System.EventArgs) Handles Button17.Click
        TextBox12.Text = ""
        TextBox11.Text = ""
        TextBox10.Text = ""
        TextBox9.Text = ""
        ComboBox6.Text = ""
        ComboBox5.Text = ""
    End Sub
End Class