﻿Imports System.Data.Odbc
Public Class mystudent
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
 
    
    Sub koneksi()
        lokasiDB = "Dsn=dsn_tendbil;server=localhost;uid=root;database=db_tendbil;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub
    Public Sub addDGV()


        With DataGridView1
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 70
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 286
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 100
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 100
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).Width = 100
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(5).Width = 100
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
           
        End With
    End Sub

    Public Sub addDGV2()


        With DataGridView1
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 70
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 786
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
           

        End With
    End Sub
    Public Sub addDGV3()


        With DataGridView2
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 86
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 300
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter


        End With
    End Sub
    Sub refreshpage()
        Call koneksi()

        da = New OdbcDataAdapter("select tb_student.username,tb_student.nama,tb_grade.grade1,tb_grade.grade2,tb_grade.grade3,tb_grade.overall from `tb_student` left join tb_grade on tb_student.id_student = tb_grade.id_student where tb_grade.id_class = '" & Label10.Text & "' ", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "`tb_student`")
        DataGridView1.DataSource = (ds.Tables("`tb_student`"))
        Call addDGV()
        TextBox2.Width = 286
        TextBox3.Visible = True
        TextBox4.Visible = True
        TextBox5.Visible = True
        TextBox6.Visible = True
    End Sub
    Sub refreshpage2()
        Call koneksi()

        CheckBox1.Checked = False
        da = New OdbcDataAdapter("select username , nama from `tb_student` where id_class = '" & Label10.Text & "' ", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "`tb_student`")
        DataGridView1.DataSource = (ds.Tables("`tb_student`"))
        Call addDGV2()
        TextBox2.Width = 726
        TextBox3.Visible = False
        TextBox4.Visible = False
        TextBox5.Visible = False
        TextBox6.Visible = False
        GroupBox1.Visible = False
        Button1.BackColor = Color.IndianRed
        Button2.BackColor = Color.Indigo
    End Sub
    Sub refreshpage3()
        Call koneksi()

        da = New OdbcDataAdapter("select username , nama from `tb_student` where id_class = '" & Label10.Text & "' ", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "`tb_student`")
        DataGridView2.DataSource = (ds.Tables("`tb_student`"))
        Call addDGV3()
     
    End Sub
    Private Sub mystudent_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Call refreshpage2()
        Call refreshpage3()
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            Call refreshpage()
        Else
            Call refreshpage2()
        End If
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        GroupBox1.Visible = True
        Button2.BackColor = Color.IndianRed
        Button1.BackColor = Color.Indigo
        TextBox12.Text = ""
        TextBox7.Text = ""
        TextBox8.Text = ""
        TextBox9.Text = ""
        TextBox10.Text = ""
        TextBox11.Text = ""
        Label6.Text = ""
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        GroupBox1.Visible = False
        Button1.BackColor = Color.IndianRed
        Button2.BackColor = Color.Indigo
    End Sub

    Private Sub DataGridView2_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView2.CellClick
     
        Dim a As Object = DataGridView2.Rows(e.RowIndex).Cells(0).Value 'username
        TextBox7.Text = a
        Dim b As Object = DataGridView2.Rows(e.RowIndex).Cells(1).Value 'name
        TextBox12.Text = b

        Call koneksi()
        cmd = New OdbcCommand("select tb_student.id_student ,tb_grade.grade1,tb_grade.grade2,tb_grade.grade3 from tb_student left join tb_grade on tb_student.id_student = tb_grade.id_student where tb_student.username = '" & a & "'", conn)
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            Label6.Text = rd.Item("id_student")
            TextBox8.Text = rd.Item("grade1").ToString
            TextBox9.Text = rd.Item("grade2").ToString
            TextBox10.Text = rd.Item("grade3").ToString
            TextBox11.Text = Val(TextBox8.Text) + Val(TextBox9.Text) + Val(TextBox10.Text)
        End If

    End Sub

    Private Sub TextBox8_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox8.KeyPress
        'validasi angka
        Dim strValid As String = "0123456789-.,"

        Dim x As Long = InStr(strValid, e.KeyChar)

        If x = 0 And Asc(e.KeyChar) <> 8 And Asc(e.KeyChar) <> 32 Then

            e.KeyChar = ""

        End If
    End Sub

    Private Sub TextBox8_TextChanged(sender As System.Object, e As System.EventArgs) Handles TextBox8.TextChanged
        TextBox11.Text = Val(TextBox8.Text) + Val(TextBox9.Text) + Val(TextBox10.Text)
    End Sub

    Private Sub TextBox9_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox9.KeyPress
        'validasi angka
        Dim strValid As String = "0123456789-.,"

        Dim x As Long = InStr(strValid, e.KeyChar)

        If x = 0 And Asc(e.KeyChar) <> 8 And Asc(e.KeyChar) <> 32 Then

            e.KeyChar = ""

        End If
    End Sub

    Private Sub TextBox9_TextChanged(sender As System.Object, e As System.EventArgs) Handles TextBox9.TextChanged
        TextBox11.Text = Val(TextBox8.Text) + Val(TextBox9.Text) + Val(TextBox10.Text)

    End Sub

    Private Sub TextBox10_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox10.KeyPress
        'validasi angka
        Dim strValid As String = "0123456789-.,"

        Dim x As Long = InStr(strValid, e.KeyChar)

        If x = 0 And Asc(e.KeyChar) <> 8 And Asc(e.KeyChar) <> 32 Then

            e.KeyChar = ""

        End If
    End Sub

    Private Sub TextBox10_TextChanged(sender As System.Object, e As System.EventArgs) Handles TextBox10.TextChanged
        TextBox11.Text = Val(TextBox8.Text) + Val(TextBox9.Text) + Val(TextBox10.Text)

    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
      
            Call koneksi()
            Dim update As String = "update tb_grade set id_student = '" & Label6.Text & "' , grade1 = '" & TextBox8.Text & "' , grade2 = '" & TextBox9.Text & "', grade3 = '" & TextBox10.Text & "', overall = '" & TextBox11.Text & "' where id_grade = '" & Label6.Text & "'"
        cmd = New OdbcCommand(update, conn)
            cmd.ExecuteNonQuery()
        MsgBox("Grade has been inserted!", MsgBoxStyle.Information)
        Call refreshpage2()

    End Sub


End Class