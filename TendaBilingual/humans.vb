﻿Imports System.Data.Odbc
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text
Public Class humans
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub koneksi()
        lokasiDB = "Dsn=dsn_tendbil;server=localhost;uid=root;database=db_tendbil;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub
    Sub refreshpage()
        ComboBox1.Text = ""
        ComboBox5.Text = ""
        ComboBox2.Text = ""
        ComboBox3.Text = ""
        dgv.DataSource = ""
        dgv2.DataSource = ""
        dgv3.datasource = ""
        dgv4.DataSource = ""
        TextBox8.Enabled = False
        TextBox16.Enabled = False
        TextBox24.Enabled = False
        TextBox32.Enabled = False
        Button1.BackColor = Color.IndianRed
        Button2.BackColor = Color.Indigo
        Button3.BackColor = Color.Indigo
        Button4.BackColor = Color.Indigo
        gbstd.Visible = True
        gbtch.Visible = False
        gbadm.Visible = False
        gbgrd.visible = False
    End Sub
    Public Sub addDGVstd()


        With dgv
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 30
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 150
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 185
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 100
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).Width = 180
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(5).Width = 30
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(6).Width = 100
            .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        End With
    End Sub
    Public Sub addDGVadm()


        With dgv3
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 30
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 150
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 185
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 100
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).Width = 180
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(5).Width = 30
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(6).Width = 100
            .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        End With
    End Sub
    Public Sub addDGVtch()


        With dgv2
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 30
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 150
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 185
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 100
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).Width = 180
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(5).Width = 30
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(6).Width = 100
            .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        End With
    End Sub
    Public Sub addDGVgrd()


        With dgv4
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 70
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 286
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 100
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 100
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).Width = 100
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(5).Width = 100
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        End With
    End Sub
    Private Sub humans_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Call refreshpage()
        Call koneksi()

        cmd = New OdbcCommand("SELECT * FROM tb_class", conn)
        rd = cmd.ExecuteReader
        While rd.Read()
            ComboBox5.Items.Add(rd.Item("nama_class"))
            ComboBox1.Items.Add(rd.Item("nama_class"))
            ComboBox3.Items.Add(rd.Item("nama_class"))
        End While
        rd.Close()
    End Sub

    Private Sub ComboBox5_Click(sender As Object, e As System.EventArgs) Handles ComboBox5.Click
        ComboBox5.Text = ""
        dgv.DataSource = ""
        TextBox8.Enabled = False
        Label2.Text = ""
    End Sub

    Private Sub ComboBox5_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox5.SelectedIndexChanged
        Call koneksi()
        cmd = New OdbcCommand("select id_class,kode_class from `tb_class` where nama_class  = '" & ComboBox5.Text & "' ", conn)
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            Label2.Text = rd.Item("id_class")
        End If
        da = New OdbcDataAdapter("select id_student,nama,alamat,no_hp,email,jk,username from `tb_student` where id_class = '" & Label2.Text & "'", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "`tb_student`")
        dgv.DataSource = (ds.Tables("`tb_student`"))
        TextBox8.Enabled = True

        Call addDGVstd()
    End Sub
    Private Sub TextBox8_GotFocus(sender As Object, e As System.EventArgs) Handles TextBox8.GotFocus
        TextBox8.Text = ""
    End Sub

    Private Sub TextBox8_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox8.KeyPress
        If TextBox8.Text = "" Then
            Call koneksi()
            cmd = New OdbcCommand("select * from tb_student", conn)
            rd = cmd.ExecuteReader
            rd.Read()
            If rd.HasRows Then
                da = New OdbcDataAdapter("select id_student,nama,alamat,no_hp,email,jk,username from `tb_student` where nama like '%" & TextBox8.Text & "%'", conn)
                ds = New DataSet
                ds.Clear()
                da.Fill(ds, "`tb_student`")
                dgv.DataSource = ds.Tables("`tb_student`")
            End If
        Else

            Call koneksi()
            cmd = New OdbcCommand("select * from tb_student where nama like'%" & TextBox8.Text & "%'", conn)
            rd = cmd.ExecuteReader
            rd.Read()
            If rd.HasRows Then
                da = New OdbcDataAdapter("select id_student,nama,alamat,no_hp,email,jk,username from `tb_student` where nama like '%" & TextBox8.Text & "%'", conn)
                ds = New DataSet
                ds.Clear()
                da.Fill(ds, "`tb_student`")
                dgv.DataSource = ds.Tables("`tb_student`")
            End If
        End If
    End Sub

    Private Sub TextBox8_LostFocus(sender As Object, e As System.EventArgs) Handles TextBox8.LostFocus
        TextBox8.Text = "Find"
    End Sub


    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Button1.BackColor = Color.IndianRed
        Button2.BackColor = Color.Indigo
        Button3.BackColor = Color.Indigo
        Button4.BackColor = Color.Indigo
        gbstd.Visible = True
        gbtch.Visible = False
        gbadm.Visible = False
        gbgrd.visible = False
    End Sub

    Private Sub ComboBox1_Click(sender As Object, e As System.EventArgs) Handles ComboBox1.Click
        ComboBox1.Text = ""
        dgv2.DataSource = ""
        TextBox16.Enabled = False
        Label1.Text = ""
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Call koneksi()
        cmd = New OdbcCommand("select id_class,kode_class from `tb_class` where nama_class  = '" & ComboBox1.Text & "' ", conn)
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            Label1.Text = rd.Item("id_class")
        End If
        da = New OdbcDataAdapter("select id_teacher,nama,alamat,no_hp,email,jk,username from `tb_teacher` where id_class = '" & Label1.Text & "'", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "`tb_teacher`")
        dgv2.DataSource = (ds.Tables("`tb_teacher`"))
        TextBox16.Enabled = True

        Call addDGVtch()
    End Sub
    Private Sub TextBox16_GotFocus(sender As Object, e As System.EventArgs) Handles TextBox16.GotFocus
        TextBox16.Text = ""
    End Sub

    Private Sub TextBox16_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox16.KeyPress
        If TextBox16.Text = "" Then
            Call koneksi()
            cmd = New OdbcCommand("select * from tb_teacher", conn)
            rd = cmd.ExecuteReader
            rd.Read()
            If rd.HasRows Then
                da = New OdbcDataAdapter("select id_teacher,nama,alamat,no_hp,email,jk,username from `tb_teacher` where nama like '%" & TextBox16.Text & "%'", conn)
                ds = New DataSet
                ds.Clear()
                da.Fill(ds, "`tb_teacher`")
                dgv2.DataSource = ds.Tables("`tb_teacher`")
            End If
        Else

            Call koneksi()
            cmd = New OdbcCommand("select * from tb_teacher where nama like'%" & TextBox16.Text & "%'", conn)
            rd = cmd.ExecuteReader
            rd.Read()
            If rd.HasRows Then
                da = New OdbcDataAdapter("select id_teacher,nama,alamat,no_hp,email,jk,username from `tb_teacher` where nama like '%" & TextBox16.Text & "%'", conn)
                ds = New DataSet
                ds.Clear()
                da.Fill(ds, "`tb_teacher`")
                dgv2.DataSource = ds.Tables("`tb_teacher`")
            End If
        End If
    End Sub

    Private Sub TextBox16_LostFocus(sender As Object, e As System.EventArgs) Handles TextBox16.LostFocus
        TextBox16.Text = "Find"
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Button2.BackColor = Color.IndianRed
        Button1.BackColor = Color.Indigo
        Button3.BackColor = Color.Indigo
        Button4.BackColor = Color.Indigo
        gbtch.Visible = True
        gbstd.Visible = False
        gbadm.Visible = False
        gbgrd.visible = False
    End Sub

    Private Sub ComboBox2_Click(sender As Object, e As System.EventArgs) Handles ComboBox2.Click
        ComboBox2.Text = ""
        TextBox24.Enabled = False
        dgv3.DataSource = ""
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Call koneksi()
        da = New OdbcDataAdapter("select id_users,nama,alamat,no_hp,email,jk,username from `tb_admin` where level = '" & ComboBox2.Text & "'", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "`tb_admin`")
        dgv3.DataSource = (ds.Tables("`tb_admin`"))
        TextBox24.Enabled = True

        Call addDGVadm()
    End Sub
    Private Sub TextBox24_GotFocus(sender As Object, e As System.EventArgs) Handles TextBox24.GotFocus
        TextBox24.Text = ""
    End Sub

    Private Sub TextBox24_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox24.KeyPress
        If TextBox24.Text = "" Then
            Call koneksi()
            cmd = New OdbcCommand("select * from tb_admin", conn)
            rd = cmd.ExecuteReader
            rd.Read()
            If rd.HasRows Then
                da = New OdbcDataAdapter("select id_admin,nama,alamat,no_hp,email,jk,username from `tb_admin` where nama like '%" & TextBox24.Text & "%'", conn)
                ds = New DataSet
                ds.Clear()
                da.Fill(ds, "`tb_admin`")
                dgv3.DataSource = ds.Tables("`tb_admin`")
            End If
        Else

            Call koneksi()
            cmd = New OdbcCommand("select * from tb_admin where nama like'%" & TextBox24.Text & "%'", conn)
            rd = cmd.ExecuteReader
            rd.Read()
            If rd.HasRows Then
                da = New OdbcDataAdapter("select id_admin,nama,alamat,no_hp,email,jk,username from `tb_admin` where nama like '%" & TextBox24.Text & "%'", conn)
                ds = New DataSet
                ds.Clear()
                da.Fill(ds, "`tb_admin`")
                dgv3.DataSource = ds.Tables("`tb_admin`")
            End If
        End If
    End Sub

    Private Sub TextBox24_LostFocus(sender As Object, e As System.EventArgs) Handles TextBox24.LostFocus
        TextBox24.Text = "Find"
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        Button3.BackColor = Color.IndianRed
        Button1.BackColor = Color.Indigo
        Button2.BackColor = Color.Indigo
        Button4.BackColor = Color.Indigo
        gbadm.Visible = True
        gbstd.Visible = False
        gbtch.Visible = False
        gbgrd.visible = False
    End Sub


    Private Sub ComboBox3_Click(sender As Object, e As System.EventArgs) Handles ComboBox3.Click
        ComboBox3.Text = ""
        dgv4.DataSource = ""
        TextBox32.Enabled = False
        Label6.Text = ""
    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox3.SelectedIndexChanged
           Call koneksi()
        Call koneksi()
        cmd = New OdbcCommand("select id_class,kode_class from `tb_class` where nama_class  = '" & ComboBox3.Text & "' ", conn)
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            Label6.Text = rd.Item("id_class")
        End If
        da = New OdbcDataAdapter("select tb_student.username,tb_student.nama,tb_grade.grade1,tb_grade.grade2,tb_grade.grade3,tb_grade.overall from `tb_student` left join tb_grade on tb_student.id_student = tb_grade.id_student where tb_student.id_class = '" & Label6.Text & "' ", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "`tb_student`")
        dgv4.DataSource = (ds.Tables("`tb_student`"))
        Call addDGVgrd()
        TextBox32.Enabled = True
       
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        Button4.BackColor = Color.IndianRed
        Button1.BackColor = Color.Indigo
        Button2.BackColor = Color.Indigo
        Button3.BackColor = Color.Indigo
        gbgrd.Visible = True
        gbstd.Visible = False
        gbtch.Visible = False
        gbadm.Visible = False
    End Sub
    Private Sub TextBox32_GotFocus(sender As Object, e As System.EventArgs) Handles TextBox32.GotFocus
        TextBox32.Text = ""
    End Sub
    Private Sub TextBox32_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox32.KeyPress
        If TextBox32.Text = "" Then
            Call koneksi()
            cmd = New OdbcCommand("select tb_student.username,tb_student.nama,tb_grade.grade1,tb_grade.grade2,tb_grade.grade3,tb_grade.overall from `tb_student` left join tb_grade on tb_student.id_student = tb_grade.id_student where tb_student.id_class = '" & Label6.Text & "'", conn)
            rd = cmd.ExecuteReader
            rd.Read()
            If rd.HasRows Then
                da = New OdbcDataAdapter("select tb_student.username,tb_student.nama,tb_grade.grade1,tb_grade.grade2,tb_grade.grade3,tb_grade.overall from `tb_student` left join tb_grade on tb_student.id_student = tb_grade.id_student where tb_student.id_class = '" & Label6.Text & "'", conn)
                ds = New DataSet
                ds.Clear()
                da.Fill(ds, "`tb_student`")
                dgv4.DataSource = ds.Tables("`tb_student`")
            End If
        Else

            Call koneksi()
            cmd = New OdbcCommand("select tb_student.username,tb_student.nama,tb_grade.grade1,tb_grade.grade2,tb_grade.grade3,tb_grade.overall from `tb_student` left join tb_grade on tb_student.id_student = tb_grade.id_student where tb_student.nama like'%" & TextBox32.Text & "%' and  tb_student.id_class = '" & Label6.Text & "'", conn)
            rd = cmd.ExecuteReader
            rd.Read()
            If rd.HasRows Then
                da = New OdbcDataAdapter("select tb_student.username,tb_student.nama,tb_grade.grade1,tb_grade.grade2,tb_grade.grade3,tb_grade.overall from `tb_student` left join tb_grade on tb_student.id_student = tb_grade.id_student where tb_student.nama like'%" & TextBox32.Text & "%' and  tb_student.id_class = '" & Label6.Text & "'", conn)
                ds = New DataSet
                ds.Clear()
                da.Fill(ds, "`tb_student`")
                dgv4.DataSource = ds.Tables("`tb_student`")
            End If
        End If
    End Sub
  

    Private Sub TextBox32_LostFocus(sender As Object, e As System.EventArgs) Handles TextBox32.LostFocus
        TextBox32.Text = "Find"
    End Sub

End Class