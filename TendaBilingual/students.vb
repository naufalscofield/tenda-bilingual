﻿Imports System.Data.Odbc
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text
Public Class students
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub koneksi()
        lokasiDB = "Dsn=dsn_tendbil;server=localhost;uid=root;database=db_tendbil;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub
    Sub refreshpage()
        ComboBox5.Text = ""
        Button1.Visible = False
        Button2.Visible = False
        Button3.Visible = False
        Button4.Visible = False
        Label1.Text = ""
        Label2.Text = ""
        Label3.Text = ""
        Label5.Text = ""
        Label7.Text = ""
        Label6.Text = "-"
        dgv.DataSource = ""
        TextBox8.Enabled = False
    End Sub
    Public Sub addDGV()


        With dgv
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 30
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 150
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 185
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 100
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).Width = 180
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(5).Width = 30
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(6).Width = 100
            .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        End With
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellClick
        Button1.Visible = True
        Button2.Visible = True

        Dim a As Object = dgv.Rows(e.RowIndex).Cells(0).Value 'id
        Label1.Text = a
        Label10.Text = a
        Dim c As Object = dgv.Rows(e.RowIndex).Cells(1).Value 'nama
        TextBox12.Text = c
        Label6.Text = c
        Dim d As Object = dgv.Rows(e.RowIndex).Cells(2).Value 'alamat
        TextBox11.Text = d
        Dim f As Object = dgv.Rows(e.RowIndex).Cells(3).Value 'nohp
        TextBox10.Text = f
        Dim g As Object = dgv.Rows(e.RowIndex).Cells(4).Value 'email
        TextBox9.Text = g
        Dim h As Object = dgv.Rows(e.RowIndex).Cells(5).Value 'jk
        ComboBox6.Text = h
        Dim b As Object = dgv.Rows(e.RowIndex).Cells(6).Value 'username
        Label5.Text = b

        ComboBox1.Text = ComboBox5.Text 'kelas

        If Label5.Text = "" Then
            Button3.Visible = True
        Else
            Button3.Visible = False
        End If

    End Sub
  Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        Dim pesan As String = MsgBox("Are you sure want delete all student's data in " & ComboBox5.Text & "?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
        If pesan = vbYes Then
            Dim hapus As String = "delete from `tb_student` where id_class = '" & Label2.Text & "'"
            cmd = New OdbcCommand(hapus, conn)
            cmd.ExecuteNonQuery()
            Dim hapus2 As String = "delete from `tb_grade` where id_class = '" & Label2.Text & "'"
            cmd = New OdbcCommand(hapus2, conn)
            cmd.ExecuteNonQuery()
            MsgBox("Data deleted!", MsgBoxStyle.Information, )
            Call refreshpage()
        End If
    End Sub

    Private Sub classes_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ComboBox5.Text = ""
        Call koneksi()

        cmd = New OdbcCommand("SELECT * FROM tb_class", conn)
        rd = cmd.ExecuteReader
        While rd.Read()
            ComboBox5.Items.Add(rd.Item("nama_class"))
            ComboBox1.Items.Add(rd.Item("nama_class"))
        End While
        rd.Close()
    End Sub

    Private Sub ComboBox5_Click(sender As Object, e As System.EventArgs) Handles ComboBox5.Click
          Call refreshpage()
    End Sub

    Private Sub ComboBox5_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox5.SelectedIndexChanged
        Call koneksi()
        cmd = New OdbcCommand("select id_class,kode_class from `tb_class` where nama_class  = '" & ComboBox5.Text & "' ", conn)
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            Label2.Text = rd.Item("id_class")
            Label3.Text = rd.Item("kode_class")
            Label8.Text = rd.Item("id_class")
        End If

      

            da = New OdbcDataAdapter("select id_student,nama,alamat,no_hp,email,jk,username from `tb_student` where id_class = '" & Label2.Text & "'", conn)
            ds = New DataSet
            ds.Clear()
            da.Fill(ds, "`tb_student`")
            dgv.DataSource = (ds.Tables("`tb_student`"))
            Button4.Visible = True
            TextBox8.Enabled = True

            Call addDGV()


    End Sub


    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        gbedit.Visible = True
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Dim pesan As String = MsgBox("Are you sure want delete student data with name " & Label6.Text & "?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
        If pesan = vbYes Then
            Dim hapus As String = "delete from `tb_student` where id_student = '" & Label1.Text & "'"
            cmd = New OdbcCommand(hapus, conn)
            cmd.ExecuteNonQuery()
            Dim hapus2 As String = "delete from `tb_grade` where id_student = '" & Label1.Text & "'"
            cmd = New OdbcCommand(hapus2, conn)
            cmd.ExecuteNonQuery()
            MsgBox("Data deleted!", MsgBoxStyle.Information, )
                Call refreshpage()
        End If
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        Label7.Text = Label4.Text + Label1.Text + Label3.Text
        Call koneksi()
        Dim edit As String = "UPDATE `tb_student` SET username ='" & Label7.Text & "' , password = '" & Label7.Text & "' where `id_student` = '" & Label1.Text & "'"
        cmd = New OdbcCommand(edit, conn)
        cmd.ExecuteNonQuery()
        MsgBox("Username has been generated,Your Username --> " & Label7.Text, MsgBoxStyle.Information, "Information")
            Call refreshpage()
    End Sub

   
    Private Sub Button13_Click(sender As System.Object, e As System.EventArgs) Handles Button13.Click
        TextBox13.Visible = False
        Button13.Visible = False
        TextBox11.Text = TextBox13.Text
    End Sub

    Private Sub Button17_Click(sender As System.Object, e As System.EventArgs) Handles Button17.Click
        TextBox12.Text = ""
        TextBox11.Text = ""
        TextBox10.Text = ""
        TextBox9.Text = ""
        ComboBox6.Text = ""
        ComboBox1.Text = ""
    End Sub

    Private Sub Button16_Click(sender As System.Object, e As System.EventArgs) Handles Button16.Click
        Call koneksi()
        Dim msg As String = MsgBox("Are you sure those data is valid?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
        If msg = vbYes Then
            Dim edit As String = "UPDATE `tb_student` SET nama ='" & TextBox12.Text & "' , alamat = '" & TextBox11.Text & "' , no_hp = '" & TextBox10.Text & "',email = '" & TextBox9.Text & "',jk = '" & ComboBox6.Text & "' ,id_class = '" & Label2.Text & "' where `id_student` = '" & Label1.Text & "'"
            cmd = New OdbcCommand(edit, conn)
            cmd.ExecuteNonQuery()
            MsgBox("Data has been edited", MsgBoxStyle.Information, "Information")
            gbedit.Visible = False
            Call refreshpage()
        End If
    End Sub

    Private Sub Label9_Click(sender As System.Object, e As System.EventArgs) Handles Label9.Click
        gbedit.Visible = False
    End Sub

    Private Sub Label15_Click_1(sender As System.Object, e As System.EventArgs) Handles Label15.Click
        TextBox13.Text = TextBox11.Text
        Button13.Visible = True
        TextBox13.Visible = True
    End Sub
    Private Sub ComboBox1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Call koneksi()
        cmd = New OdbcCommand("select id_class,kode_class from `tb_class` where nama_class  = '" & ComboBox1.Text & "' ", conn)
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            Label8.Text = rd.Item("id_class")
        End If
    End Sub

    Private Sub TextBox8_GotFocus(sender As Object, e As System.EventArgs) Handles TextBox8.GotFocus
        TextBox8.Text = ""
    End Sub

    Private Sub TextBox8_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox8.KeyPress
        If TextBox8.Text = "" Then
            Call koneksi()
            cmd = New OdbcCommand("select * from tb_student where id_class = '" & Label2.Text & "' ", conn)
            rd = cmd.ExecuteReader
            rd.Read()
            If rd.HasRows Then
                da = New OdbcDataAdapter("select id_student,nama,alamat,no_hp,email,jk,username from `tb_student` where nama like '%" & TextBox8.Text & "%' and id_class = '" & Label2.Text & "'", conn)
                ds = New DataSet
                ds.Clear()
                da.Fill(ds, "`tb_student`")
                dgv.DataSource = ds.Tables("`tb_student`")
            End If
        Else

            Call koneksi()
            cmd = New OdbcCommand("select * from tb_student where nama like'%" & TextBox8.Text & "%' and id_class = '" & Label2.Text & "'", conn)
            rd = cmd.ExecuteReader
            rd.Read()
            If rd.HasRows Then
                da = New OdbcDataAdapter("select id_student,nama,alamat,no_hp,email,jk,username from `tb_student` where nama like '%" & TextBox8.Text & "%' and id_class = '" & Label2.Text & "'", conn)
                ds = New DataSet
                ds.Clear()
                da.Fill(ds, "`tb_student`")
                dgv.DataSource = ds.Tables("`tb_student`")
            End If
        End If
    End Sub

    Private Sub TextBox8_LostFocus(sender As Object, e As System.EventArgs) Handles TextBox8.LostFocus
        TextBox8.Text = "Find"
    End Sub


    Private Sub TextBox10_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox10.KeyPress
        'validasi angka
        Dim strValid As String = "0123456789-.,"

        Dim x As Long = InStr(strValid, e.KeyChar)

        If x = 0 And Asc(e.KeyChar) <> 8 And Asc(e.KeyChar) <> 32 Then

            e.KeyChar = ""

        End If
    End Sub

    Private Sub Panel1_Paint(sender As System.Object, e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub
End Class